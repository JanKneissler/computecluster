kubectl create job cuda \
--image=kneissler/cuda-ml \
--env="LD_LIBRARY_PATH=/usr/local/nvidia/lib64:/usr/local/nvidia/bin" \
--limits="nvidia.com/gpu=1" \
-- python trainTest.py
