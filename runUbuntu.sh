kubectl run cuda \
--image=ubuntu:16.04 \
--env="LD_LIBRARY_PATH=/usr/local/nvidia/lib64:/usr/local/nvidia/bin" \
--limits="nvidia.com/gpu=1" \
--rm -it -- /bin/bash
