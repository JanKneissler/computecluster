kubectl run cuda$1 \
--image=kneissler/cuda-ml \
--env="LD_LIBRARY_PATH=/usr/local/nvidia/lib64:/usr/local/nvidia/bin" \
--limits="nvidia.com/gpu=1" \
-- \
train.sh 
