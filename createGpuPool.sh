ZONE=west3-b
echo Available gpus for $ZONE
gcloud beta compute accelerator-types list | grep $ZONE


#gcloud beta container node-pools \
#create gpu-pool \
#--num-nodes=1 \
#--accelerator type=nvidia-tesla-k80,count=1 \
#--zone europe-west1-d  \
#--cluster k8s-gpu

echo Creating node pool
gcloud beta container \
   --project "deep-learning-265108" \
   node-pools create "gpu-nodes2" \
   --cluster "gpu-cluster" \
   --zone "europe-west3-b" \
   --node-version "1.14.10-gke.27" \
   --machine-type "n1-standard-1" \
   --accelerator "type=nvidia-tesla-t4,count=1" \
   --image-type "COS" \
   --disk-type "pd-standard" \
   --disk-size "100" \
   --metadata disable-legacy-endpoints=true \
   --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
   --preemptible \
   --num-nodes "1" \
   --enable-autoscaling \
   --min-nodes "0" \
   --max-nodes "3" \
   --enable-autoupgrade \
   --enable-autorepair

echo "Setting up GPU drivers"
kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/container-engine-accelerators/master/nvidia-driver-installer/cos/daemonset-preloaded.yaml
